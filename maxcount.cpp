#include<cstdio>
#include<map>
#include<stdexcept>

using namespace std;

int main()
{
  int t;
  int n;
  int var;
  scanf("%d",&t);
  for(int i=0;i<t;i++)
  {
    scanf("%d",&n);
    map<int,int> count;
    for(int j=0;j<n;j++)
    {
      scanf("%d",&var);
      // check if var is present in the map
      try
      {
        count.at(var) += 1;
      }
      catch(const out_of_range& error)
      {
        count.insert(pair<int,int>(var,1));
      }
    }
    int var = 0;
    int times = 0;

    for(map<int, int>::iterator it = count.begin(); it != count.end(); it++)
    {
      if(times < it->second)
      {
        var = it->first;
        times = it->second;
      }
      else if(times == it->second)
      {
        var = (var<it->first?var:it->first);
      }
    }
    printf("%d %d\n",var, times);
  }
  return 0;
}
