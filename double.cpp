#include<cstdio>

long getDoubleStrings(long);

int main()
{
  int t;
  long n;

  scanf("%d",&t);

  for(int i = 0;i < t;i++)
  {
    scanf("%ld",&n);
    printf("%ld\n",getDoubleStrings(n));
  }
  return 0;
}

long getDoubleStrings(long n)
{
  if(n%2 == 0)
  {
    return n;
  }
  else
  {
    return n-1;
  }
}
