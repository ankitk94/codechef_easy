#include<vector>
#include<stack>
#include<set>
#include<bitset>
#include<map>
#include<queue>
#include<deque>
#include<string>


//Other Includes
#include<iostream>
#include<fstream>
#include<algorithm>
#include<cstring>
#include<cassert>
#include<cstdlib>
#include<cstdio>
#include<cmath>

using namespace std;

#define FOR(i,a,b) for(int i=a;i<b;i++)
#define REP(i,n) FOR(i,0,n)
#define s(n) scanf("%d",&n)
/*Main Code*/
int main()
{
  //freopen("in.txt","r",stdin);
  int t,n;
  for(s(t);t--;)
  {
    s(n);
    int pie[101]={0};
    int rack[101]={0},done[101]={0};
    REP(i,n)s(pie[i]);
    REP(i,n)s(rack[i]);
    sort(pie, pie+n);
    sort(rack,rack+n);
    int count =0;
    REP(i,n)
      REP(j,n)
      {
        if(done[j]==0)
          if(rack[j]>=pie[i])
          {
            done[j]=1;
            count++;
            break;
          }
      }
    cout<<count<<"\n";
  }
  return 0;
} 
