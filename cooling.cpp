#include<cstdio>
#include<algorithm>

int getCount();

int main()
{
  int t;
  scanf("%d",&t);

  for(int i=0;i<t;i++)
  {
    printf("%d\n",getCount());
  }
  return 0;
}

int getCount()
{
  int n;
  int weights[30];
  int limits[30];

  scanf("%d",&n);
  for(int j=0;j<n;j++)
    scanf("%d",&weights[j]);
  int max_limit = -1;
  for(int j=0;j<n;j++)
  {
    scanf("%d",&limits[j]);
    if(max_limit < limits[j])
      max_limit = limits[j];
  }
  std::sort(weights, weights+n);
  std::sort(limits, limits+n);
  int count = 0;
  int index = 0;
  for(int i=0;i<n;i++)
  {
    if(limits[i] >= weights[index])
    {
      count++;
      index++;
    }
  }
  return count;
}
