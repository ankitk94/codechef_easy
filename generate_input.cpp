#include<iostream>
#include<fstream>
#include<cstdlib>
#include<string>
#include<sstream>
#include<ctime>

using namespace std;

string to_string(int num)
{
  stringstream convert;
  convert << num;
  return convert.str();
}

int main()
{
  // first generate the string to be written to file
  string write = "";

  srand(time(NULL));
  int t = rand()%30 + 1;
  
  write += to_string(t) + "\n";

  for(int i=0;i<t;i++)
  {
    int n = rand()%30 + 1;
    write += to_string(n) + "\n";

    // generate the weights

    string temp = "";

    for(int j = 0;j<n;j++)
    {

      temp = temp + to_string(rand()%100 + 1) + " ";
    }

    temp += "\n";

    //generate the limits

    for(int j = 0;j<n;j++)
    {

      temp = temp + to_string(rand()%100 + 1) + " ";
    }

    temp += "\n";

    write += temp;
  }

  ofstream file;
  file.open("input");
  file << write;
  file.close();
  return 0;
}
