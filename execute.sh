#!/bin/bash

# generate an input file
g++ generate_input.cpp -o generate_input.out
./generate_input.out

g++ cooling.cpp -o cooling.out
g++ copied_cooling.cpp -o copied_cooling.out

./cooling.out < input > output1
./copied_cooling.out < input > output2

var=$(diff output1 output2)

echo $var
